module gitee.com/iotas/iota

go 1.16

require (
	gitee.com/iotas/toolkit v0.0.0-20211214111700-543e62e58d07
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.9.0
	github.com/json-iterator/go v1.1.12
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	google.golang.org/protobuf v1.27.1
)
